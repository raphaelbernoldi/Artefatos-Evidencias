CREATE DATABASE Estacionamento;

USE Estacionamento;

CREATE TABLE `tbl_preco` (
  `id` INT NOT NULL,
  `marca` VARCHAR(255) NULL DEFAULT 'null',
  `modelo` VARCHAR(255) NULL DEFAULT 'null',
  `preco` VARCHAR(255) NULL DEFAULT 0,
  PRIMARY KEY (`id`));


-- CHEVROLET
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(1,'GM', 'BLAZER', '20.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(2,'GM', 'COBALT', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(3,'GM', 'CRUZE', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(4,'GM', 'AGILE', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(5,'GM', 'CORSA', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(6,'GM', 'CELTA', '5.00');

-- FORD
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(7,'FORD', 'FUSION', '10.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(8,'FORD', 'EDGE', '10.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(9,'FORD', 'FIESTA', '10.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(10,'FORD', 'ECOSPORT', '10.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(11,'FORD', 'RANGER', '10.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(12,'FORD', 'FOCUS', '10.00');

-- VW
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(13,'VW', 'JETTA', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(14,'VW', 'GOLF', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(15,'VW', 'GOL', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(16,'VW', 'TIGUAN', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(17,'VW', 'TOUAREG', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(18,'VW', 'PASSAT', '5.00');

-- AUDI
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(19,'AUDI', 'A1', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(20,'AUDI', 'A2', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(21,'AUDI', 'A3', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(22,'AUDI', 'A4', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(23,'AUDI', 'A5', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(24,'AUDI', 'A6', '5.00');

-- FIAT
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(25,'FIAT', 'UNO', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(26,'FIAT', 'PALIO', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(27,'FIAT', 'LINEA', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(28,'FIAT', 'SIENA', '5.00');
INSERT INTO `tbl_preco`(`id`, `marca`,	`modelo`, `preco`)VALUES(29,'FIAT', 'DOBLO', '5.00');

